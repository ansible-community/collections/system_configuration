import json, copy

forbidden_partition_names = [
    '/boot',
    '/boot/efi',
    'PReP'
]

def desired_vgs(data):
    """ Parse desired state of partitioning

Parse data structure passed as desired state of partitioning. 

Returns a list of VolumeGroup objects.

Example:

- vg: VGROOT
  lvs:
      - lv_root:
          fs: xfs
          size_g: 6
          mount: /
      - lv_swap:
          fs: swap
          size_g: 2
  pvs:
      - /dev/sda2
- vg: VGDATA
  lvs:
      - lv_data:
          fs: xfs
          size_g: 6
          mount: /data
  pvs:
      - /dev/sda3
"""
    desired_vgs = [] # list to be returned from function

    # For every VG, read and add LVs and PVs
    for vg in data:
        desired_lvs = {}
        # Loop over logical volumes
        for lv in vg['lvs']:
            # Split LV data structure into key and value part
            for key, value in lv.items():
                fs = value['fs']
                if fs == 'swap':
                    m = "swap"
                else:
                    m = value['mount']
            desired_lvs[key] = LogicalVolume(
                key,
                vg['vg'],
                fs,
                value['size_g'],
                m
            )
                
        desired_pvs = {}
        # Loop over physical volumes
        for pv in vg['pvs']:
            # TODO Lookup PV size
            desired_pvs[pv] = PhysicalVolume(
                pv,
                vg['vg']
            )

        # all components of a VG gathered, append to result list
        desired_vgs.append(VolumeGroup(
            vg['vg'],
            desired_lvs,
            desired_pvs
        )
    )
    return desired_vgs

def used_vgs(data):
    """ Parse current state of partitioning, as reported by Ansible facts

Parse data structure passed as desired state of partitioning. 

Returns a list of VolumeGroup objects.

Example:
  {
    "lvs": {
        "lv_root": {
            "size_g": "17.44",
            "vg": "VGROOT"
            },
        "lv_swap": {
            "size_g": "2.00",
            "vg": "VGROOT"
            }
    },
    "pvs": {
        "/dev/sda2": {
            "free_g": "0",
            "size_g": "9.47",
            "vg": "VGROOT"
            },
        "/dev/sda3": {
            "free_g": "0",
            "size_g": "9.97",
            "vg": "VGROOT"
            }
    },
    "vgs": {
        "VGROOT": {
            "free_g": "0",
            "num_lvs": "2",
            "num_pvs": "2",
            "size_g": "19.44"
            }
        }
    }

"""
    used_vgs = [] # list to be returned from function
    used_lvs = {} # temporary storage for used LVs
    used_pvs = {} # temporary storage for used PVs

    # Collect all PVs
    for key, value in data['pvs'].items():
        used_pvs[key] = PhysicalVolume(
            key,
            value['vg'],
            value['size_g'],
            value['free_g']
        )
    # Collect all LVs
    for key, value in data['lvs'].items():
        used_lvs[key] = LogicalVolume(
            key,
            value['vg'],
            "",
            value['size_g'],
            ""
        )

    # Scan VGs
    for key, value in data['vgs'].items():
        pvs = {}
        # Is PV used in this VG?
        for pv in used_pvs.values():
            if pv.vg == key:
                # Yes, is used, so add it 
                pvs[pv.name] = pv
        lvs = {}
        # Is LV used in vhis VG? Same as above
        for lv in used_lvs.values():
            if lv.vg == key:
                lvs[lv.name] = lv

        # Create and append VG with matching PVs and LVs
        used_vgs.append(VolumeGroup(
            key,
            lvs,
            pvs,
            value['size_g'],
            value['free_g']
        ))
        

    return used_vgs

def physical_volumes(data):
    partitions = {}
    for device, devspec in data.items():
        if 'partitions' in devspec.keys():
            for partition, partspec in devspec['partitions'].items():
                space = int(partspec['sectors'])*int(partspec['sectorsize'])/1024/1024/1024
                partitions["/dev/"+partition] = PhysicalVolume("/dev/"+partition, size=space, free=space)
    return partitions

def getMounts(data):
    mounts = {}
    for  mount in data:
        if 'device' in mount:
            mounts[mount['device']] = { 'device': mount['device'], 'fstype': mount['fstype'], 'mount': mount['mount'] }
    return mounts

class Partitioning(object):
    """ Class used to represent a partitioning scheme

    ...

    Attributes
    ----------
    vgs: List of VolumeGroup objects

    Methods
    -------
    find(obj)
        Search for an PartitioningObject
    """
    
    def __init__(self, vgs=[]):
        """
        Parameters
        ----------
        vgs: List of VolumeGroup objects (optional)
        """
        self.vgs = vgs
        if vgs != []:
            for vg in vgs:
                if not isinstance(vg, VolumeGroup):
                    raise TypeError("Partitioning accepts a list of VolumeGroups only")


    def __repr__(self):
        output = "[{0}]"
        vgs = ""
        for vg in self.vgs:
            vgs += str(vg)
            vgs += ","
        return output.format(vgs[:len(vgs)-1])

    def find(self, item):
        """Search for an PartitioningObject, return object if found, otherwise None

        Parameters
        ----------
        item: Object of subclass of class PartitioningObject

        Returns
        -------
        PartitioningObject, i.e. LogicalVolume or VolumeGroup
        """
        for v in self.vgs:
            # Determine type of search argument, and compare names
            if isinstance(item, VolumeGroup):
                if v.name == item.name:
                    return v
            if isinstance(item, LogicalVolume):
                for l in v.lvs.values():
                    if l.name == item.name:
                        return l
            if isinstance(item, PhysicalVolume):
                for p in v.pvs.values():
                    if p.name == item.name:
                        return p

        return None


class PartitioningObject(object):
    """
    Base class for objects in a class Partitioning structure

    ...

    Attributes
    ----------
    name: str
        Name of the object
    state: str
        'present' or 'absent'; set after comparision active vs. desired state

    """
    def __init__(self, name):
        """
        Parameters
        ----------
        name: str
            Name of the object
        state: str
            'present' or 'absent'; set after comparision active vs. desired state
        """
        self.name = name
        self.result = "0"
        self.state = ""
        
    def __repr__(self):
        return str(self.name)

    def __lt__(self, other):
        return self.name < other.name
    
    def __gt__(self, other):
        return self.name > other.name

        
class VolumeGroup(PartitioningObject):
    """
    Class representing a volume group, subclassed from PartitioningObject.

    ...

    Attributes
    ----------
    lvs: list of LogicalVolume objects
    pvs: list of PhysicalVolume objects
    size: float
        Volume group total size, in GB
    free: float
        Volume group free size, in GB

    Methods
    -------
    isMissing(Partitioning)
        Is this volume group present in the submitted partitioning structure
    """

    def __init__(self, name, lvs={}, pvs={}, size=-1, free=-1):
        """
        Parameters
        ----------
        lvs: dict of LogicalVolume objects (optional, default empty dict)
        pvs: dict of PhysicalVolume objects (optional, default empty dict)
        size: float
            Volume group total size, in GB (optional, default -1)
        free: float
            Volume group free size, in GB (optional, default -1)
        """
        PartitioningObject.__init__(self, name)
        if not isinstance(lvs, dict):
            raise TypeError("lvs must be a dict")
        for lv in lvs.values():
            if not isinstance(lv, LogicalVolume):
                raise TypeError("lvs must be a dict of LogicalVolumes")
        if not isinstance(pvs, dict):
            raise TypeError("pvs must be a dict of LogicalVolumes")
        for pv in pvs.values():
            if not isinstance(pv, PhysicalVolume):
                raise TypeError("pvs must be a dict of PhysicalVolumes")
        try:
            free = float(free)
        except:
            raise TypeError("Size must be a number, was {0}".format(size))
        try:
            size = float(size)
        except:
            raise TypeError("Free size must be a number, was {0}".format(free))
        if free > size:
            raise ValueError("Free size cannot be larger than total size")
        self.lvs = lvs
        self.pvs = pvs
        self.size = size
        self.free = free

    def __print__(self):
        return "VG {0}".format(self.name)

    def __repr__(self):
        return '{{ "vg": "{name}","info":{{"state":"{state}","num_lvs":"{numLvs}","num_pvs":"{numPvs}","free_g":"{free}","size_g":"{size}"}},"lvs":{lvs},"pvs":{pvs},"result":"{res}"}}'.format(
            name=self.name,
            state=self.state,
            lvs = list(self.lvs.values()),
            pvs = list(self.pvs.values()),
            size = self.size,
            free = self.free,
            numLvs = len(self.lvs),
            numPvs = len(self.pvs),
            res = self.result
        )

    def isMissing(self, partitioning):
        """
        Takes a Partitioning object and checks if that partitioning already has 
        volume group with the same name.

        Parameters
        ----------
        partitioning: Partitioning
            A partitioning structure to search in

        Returns
        -------
        True, if found
        """
        for vg in partitioning.vgs:
            if vg.name == self.name:
                return False
        return True

    def updateSize(self):
        """
        Recalulate size based on physical volumes.

        Parameters
        ----------
        None

        Returns
        -------
        Nothing
        """
        self.size = 0
        for pv in self.pvs.values():
            if pv.size != -1:
                self.size += pv.size

        self.free = self.size
        for lv in self.lvs.values():
            self.free -= lv.size
        

class PhysicalVolume(PartitioningObject):
    """
    Class representing a physical volume, subclassed from PartitioningObject.

    Parameters
    ----------
    vg: str
       Volume group (VolumeGroup object) the PV belongs to
    size: float
       Total size of physical volume in GB. Defaults to -1
    free: float
       Total free size of physical volume in GB. Defaults to -1
    """
    def __init__(self, name, vg=None, size=-1, free=-1):
        """
        Parameters
        ----------
        vg: str
            Volume group (VolumeGroup object) the PV belongs to
        size: float
            Total size of physical volume in GB. Defaults to -1
        free: float
            Total free size of physical volume in GB. Defaults to -1
    """
        PartitioningObject.__init__(self, name)
        try:
            free = float(free)
        except:
            raise TypeError("Size must be a number, was {0}".format(size))
        try:
            size = float(size)
        except:
            raise TypeError("Free size must be a number, was {0}".format(free))
        if free > size:
            raise ValueError("Free size cannot be larger than total size")
        self.vg = vg
        self.size = size
        self.free = free

    def __repr__(self):
        return '{{"{name}":{{"state":"{state}","size_g":"{size}","free_g:":"{free}"}}}}'.format(
            name = self.name,
            state = self.state,
            size = self.size,
            free = self.free)

class LogicalVolume(PartitioningObject):
    """
    Class representing a logical volume, subclassed from PartitioningObject.

    Parameters
    ----------
    vg: str
       Volume group (VolumeGroup object) the LV belongs to. Default to None
    fs: str
       Filesystem used for OS partitioning. Defaults to ""
    size: float
       Total size of physical volume in GB. Defaults to -1
    mount: str
       Mountpoint as path. Defaults to ""
    """
    def __init__(self, name, vg="", fs="", size=-1, mount=""):
        """
        Parameters
        ----------
        vg: str
            Volume group (VolumeGroup object) the LV belongs to. Default to None
        fs: str
            Filesystem used for OS partitioning. Defaults to ""
        size: float
            Total size of physical volume in GB. Defaults to -1
        mount: str
            Mountpoint as path. Defaults to ""
    """
        PartitioningObject.__init__(self, name)
        try:
            size = float(size)
        except:
            raise TypeError("Size must be a number, was {0}".format(size))
        self.vg = vg
        self.fs = fs
        self.size = size
        if mount in forbidden_partition_names:
            raise TypeError("Forbidden name for partition detected")
        self.mount = mount

    def __repr__(self):
        return '{{"{name}":{{"state":"{state}","fs":"{fs}","size_g":"{size}","mount":"{mount}"}}}}'.format(
            name = self.name,
            state = self.state,
            fs = self.fs,
            size = self.size,
            mount = self.mount)

class FilterModule(object):
    def filters(self):
        return {
            'lvmdiff': self.lvmdiff
        }

    def lvmdiff(self, used, desired, devices, mounts):
        result = {}

        partDesired = Partitioning(desired_vgs(desired))
        partUsed = Partitioning(used_vgs(used))
        partToDo = Partitioning()
        pv_present = physical_volumes(devices)
        mounts_present = getMounts(mounts)
        # Loop over actual configuration, set everything to 'absent' which is not
        # listed in the desired configuration
        vgs = partUsed.vgs # list manipulation during loop is bad...
        for vg in vgs:
            # Create a complete copy incl. all subobjects
            vg_copy = copy.deepcopy(vg)
            if vg.isMissing(partDesired):
                vg_copy.state = "absent"
                for lv in vg_copy.lvs.values():
                    lv.state = "absent"
                    mount = mounts_present.get("/dev/mapper/" + vg.name + "-" + lv.name)
                    print(mount)
                    if mount != None:
                        lv.fs = mount['fstype']
                        lv.mount = mount['mount']
                    else:
                        lv.fs = "swap"
                        lv.mount = "swap"
                for pv in vg_copy.pvs.values():
                    pv.state = "absent"
            else:
                vg_copy.state = "present"
                            
                for lv in vg_copy.lvs.values():
                    lv.state = "present"
                    # Update changed attributes, if already active
                    l = partDesired.find(lv)
                    if l != None:
                        if lv.size != l.size:
                            lv.state = "resized"
                            vg_copy.state = "resized"
                        lv.fs = l.fs
                        lv.size = l.size
                        lv.mount = l.mount
                for pv in vg_copy.pvs.values():
                    pv.state = "present"
                    # Update changed attributes, if already active
                    # p = partDesired.find(pv)
                    # if p != None and p.size > 0:
                    #     #pv.free = p.free
                    #     pv.size = p.size
                    # else :
                    #     pv.state = "extended"
                    #     vg_copy.state = "extended"

            partToDo.vgs.append(vg_copy)

        # Loop over desired items, add if not already present
        vgs = partDesired.vgs
        for vg in vgs:
            if vg.isMissing(partUsed):
                vg_copy = copy.deepcopy(vg)
                vg_copy.state = "present"
                for lv in vg_copy.lvs.values():
                    ### TODO check space
                    lv.state = "present"
                for pv in vg_copy.pvs.values():
                    pv.state = "present"
                    
                partToDo.vgs.append(vg_copy)
                
            else:
                vg_existing = partToDo.find(vg)
                for lv in vg.lvs.values():
                    l = partToDo.find(lv)
                    if l != None:
                        if lv.size != l.size:
                            l.state = "resized"
                            l.size = lv.size
                    else:
                        l = copy.deepcopy(lv)
                        l.state = "present"
                        vg_existing.state = "resized"
                        vg_existing.lvs[l.name] = l
                        
                        
                for pv in vg.pvs.values():
                    p = partToDo.find(pv)
                    if p != None:
                        if pv.size != p.size and pv.size >0:
                            p.size = pv.size
                            p.state = "resized"
                            vg_existing.state = "resized"
                    else:
                        p = copy.deepcopy(pv)
                        p.state = "present"
                        vg_existing.state = "resized"
                        vg_existing.pvs[p.name] = p

        # loop over planned partitioning, add new PVs not yet configured in LVM

        # Space requirements
        for vg in partToDo.vgs:
            # loop over PVs, add new PVs not yet configured in LVM
            for pv in vg.pvs.values():
                if pv.size == -1: # PV is not present in actual LVM
                    try:
                        pv.size = pv_present[pv.name].size
                        pv.free = pv_present[pv.name].free
                        pv.state = "resized"
                    except:
                        print("Desired PV {0} could not be found in system".format(pv.name))
                        vg.result = "1"

            vg.updateSize()
            neededSpace = 0
            for lv in vg.lvs.values():
                neededSpace += float(lv.size)

            if neededSpace > float(vg.size):
                print("Space requirement exceeded: {0} has {1} available, {2} required".format(vg.name, vg.size, neededSpace))
                vg.result = "1"
                # exit(1)
        
        
        return partToDo
    

import yaml

# Main function used during interactive testing only
if __name__ == "__main__":

    # some test data
    partitioning_lvm_configuration = """
- vg: VGROOT
  lvs:
      - lv_root:
          fs: xfs
          size_g: 17.44
          mount: /
      - lv_swap:
          fs: swap
          size_g: 2
  pvs:
      - /dev/sda2
      - /dev/sda3
- vg: VGADD
  lvs:
      - lv_data:
          fs: xfs
          size_g: 6
          mount: /data
  pvs:
      - /dev/sda6
- vg: VGEXT
  lvs:
      - lv_ch1:
          fs: xfs
          size_g: 14.5
          mount: /ch1
      - lv_ch2:
          fs: xfs
          size_g: 15.5
          mount: /ch2
  pvs:
      - /dev/sda8
      - /dev/sda9
"""

    ansible_input = """
{
    "lvs": {
        "lv_root": {
            "size_g": "17.44",
            "vg": "VGROOT"
            },
        "lv_swap": {
            "size_g": "2.00",
            "vg": "VGROOT"
            },
        "lv_extra": {
            "size_g": "2.3",
            "vg": "VGRM"
            },
        "lv_ch1": {
            "size_g": "14.5",
            "vg": "VGEXT"
            }
    },
    "pvs": {
        "/dev/sda2": {
            "free_g": "0",
            "size_g": "9.47",
            "vg": "VGROOT"
            },
        "/dev/sda3": {
            "free_g": "0",
            "size_g": "9.97",
            "vg": "VGROOT"
            },
        "/dev/sda4": {
            "free_g": "0",
            "size_g": "9.97",
            "vg": "VGRM"
            },
        "/dev/sda8": {
            "free_g": "0",
            "size_g": "14.5",
            "vg": "VGEXT"
            }
    },
    "vgs": {
        "VGROOT": {
            "free_g": "0",
            "num_lvs": "2",
            "num_pvs": "2",
            "size_g": "19.44"
            },
        "VGRM": {
            "free_g": "12.2",
            "num_lvs": "1",
            "num_pvs": "1",
            "size_g": "14.5"
            },
        "VGEXT": {
            "free_g": "0",
            "num_lvs": "1",
            "num_pvs": "1",
            "size_g": "14.5"
            }
        }
}
"""

    ansible_devices = """
{
    "dm-0": {
        "sectors": "136314880",
        "sectorsize": "512",
        "size": "65.00 GB"
    },
    "sda": {
        "holders": [],
        "partitions": {
            "sda2": {
                "holders": [],
                "sectors": "19860030",
                "sectorsize": 512,
                "size": "9.47 GB",
                "start": "2048"
            },
            "sda3": {
                "holders": [],
                "sectors": "20908606",
                "sectorsize": 512,
                "size": "9.97 GB",
                "start": "411648"
            },
            "sda4": {
                "holders": [
                    "fedora_ruriko-swap",
                    "fedora_ruriko-home",
                    "fedora_ruriko-root"
                ],
                "sectors": "20908606",
                "sectorsize": 512,
                "size": "9.97 GB",
                "start": "1435648"
            },
            "sda6": {
                "holders": [],
                "sectors": "12582912",
                "sectorsize": 512,
                "size": "6.0 GB",
                "start": "411648"
            },
            "sda8": {
                "holders": [],
                "sectors": "30408704",
                "sectorsize": 512,
                "size": "14.5 GB",
                "start": "411648"
            },
            "sda9": {
                "holders": [],
                "sectors": "32505856",
                "sectorsize": 512,
                "size": "15.5 GB",
                "start": "411648"
            }
        },
        "sectors": "137174714",
        "sectorsize": "512",
        "size": "65.41 GB"
    }
}
"""

    ansible_mounts = """
[
    {
        "fstype": "xfs",
        "mount": "/",
        "device": "/dev/mapper/VGROOT-lv_root"
    },
    {
        "fstype": "xfs",
        "mount": "/data",
        "device": "/dev/mapper/VGRM-lv_extra"
    },
    {
        "fstype": "xfs",
        "mount": "/ch1",
        "device": "/dev/mapper/VGEXT-lv_ch1"
    }
]
"""

    y = yaml.load(partitioning_lvm_configuration, Loader=yaml.FullLoader)
    j = json.loads(ansible_input)
    d = json.loads(ansible_devices)
    m = json.loads(ansible_mounts)
    f = FilterModule()
    # print(y)

    part = Partitioning(desired_vgs(y))
    res = f.lvmdiff(j, y, d, m)
    print(res)
    
