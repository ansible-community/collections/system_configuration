import unittest, json, yaml

from partitioningFilter import FilterModule, Partitioning, VolumeGroup, LogicalVolume, PhysicalVolume

current_state = """
{
        "lvs": {
            "lv_root": {
                "size_g": "11.00",
                "vg": "VG_root"
            },
            "lv_swap": {
                "size_g": "1.00",
                "vg": "VG_swap"
            }
        },
        "pvs": {
            "/dev/sda2": {
                "free_g": "0",
                "size_g": "1.00",
                "vg": "VG_swap"
            },
            "/dev/sda3": {
                "free_g": "0",
                "size_g": "11.00",
                "vg": "VG_root"
            }
        },
        "vgs": {
            "VG_root": {
                "free_g": "0",
                "num_lvs": "1",
                "num_pvs": "1",
                "size_g": "11.00"
            },
            "VG_swap": {
                "free_g": "0",
                "num_lvs": "1",
                "num_pvs": "1",
                "size_g": "1.00"
            }
        }
}
"""

desired_state = """
- vg: VG_swap
  lvs:
      - lv_swap:
          fs: swap
          size_g: 1
  pvs:
      - /dev/sda2
- vg: VG_root
  lvs:
      - lv_root:
          fs: xfs
          size_g: 11
          mount: /
  pvs:
      - /dev/sda3
- vg: VG_data
  lvs:
      - lv_data:
          fs: xfs
          size_g: 15
          mount: /data
  pvs:
      - /dev/sda4
"""

ansible_mounts = """
[
    {
        "fstype": "xfs",
        "mount": "/",
        "device": "/dev/mapper/VG_root-lv_root"
    },
    {
        "fstype": "xfs",
        "mount": "/data",
        "device": "/dev/mapper/VG_data-lv_data"
    }
]
"""

ansible_devices = """
{
        "dm-0": {
            "holders": [],
            "host": "",
            "links": {
                "ids": [
                    "dm-name-VG_root-lv_root",
                    "dm-uuid-LVM-pjtGo52XD4Hg0JVgo920Skia8wr8QYDRJ7aiyA1TG7HgICFDuicWeyPXbhPh2gHX"
                ],
                "labels": [],
                "masters": [],
                "uuids": [
                    "10f19f63-955d-4905-aa01-553cd3fb5e43"
                ]
            },
            "model": null,
            "partitions": {},
            "removable": "0",
            "rotational": "0",
            "sas_address": null,
            "sas_device_handle": null,
            "scheduler_mode": "",
            "sectors": "23060480",
            "sectorsize": "512",
            "size": "11.00 GB",
            "support_discard": "0",
            "vendor": null,
            "virtual": 1
        },
        "dm-1": {
            "holders": [],
            "host": "",
            "links": {
                "ids": [
                    "dm-name-VG_swap-lv_swap",
                    "dm-uuid-LVM-SLMEC9sr1oAeOYsIJtjJuUfoxAgw3b4Zq2KQuET70RP7XZFLOcayliHppUeIHkuD"
                ],
                "labels": [],
                "masters": [],
                "uuids": [
                    "a0441897-7376-42ae-b4a1-d57ff678e845"
                ]
            },
            "model": null,
            "partitions": {},
            "removable": "0",
            "rotational": "0",
            "sas_address": null,
            "sas_device_handle": null,
            "scheduler_mode": "",
            "sectors": "2097152",
            "sectorsize": "512",
            "size": "1.00 GB",
            "support_discard": "0",
            "vendor": null,
            "virtual": 1
        },
        "sda": {
            "holders": [],
            "host": "Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)",
            "links": {
                "ids": [],
                "labels": [],
                "masters": [],
                "uuids": []
            },
            "model": "Virtual disk",
            "partitions": {
                "sda1": {
                    "holders": [],
                    "links": {
                        "ids": [],
                        "labels": [],
                        "masters": [],
                        "uuids": [
                            "21d6307f-5ae0-4d01-a17e-a513ecd74a09"
                        ]
                    },
                    "sectors": "2097152",
                    "sectorsize": 512,
                    "size": "1.00 GB",
                    "start": "2048",
                    "uuid": "21d6307f-5ae0-4d01-a17e-a513ecd74a09"
                },
                "sda2": {
                    "holders": [
                        "VG_swap-lv_swap"
                    ],
                    "links": {
                        "ids": [
                            "lvm-pv-uuid-X8U983-TqLv-aers-VvmK-Xuz3-WFPq-qcP9Uv"
                        ],
                        "labels": [],
                        "masters": [
                            "dm-1"
                        ],
                        "uuids": []
                    },
                    "sectors": "2099200",
                    "sectorsize": 512,
                    "size": "1.00 GB",
                    "start": "2099200",
                    "uuid": null
                },
                "sda3": {
                    "holders": [
                        "VG_root-lv_root"
                    ],
                    "links": {
                        "ids": [
                            "lvm-pv-uuid-Qsy2I1-rleS-Z4ls-cF5d-tKv2-mQxk-lokeCi"
                        ],
                        "labels": [],
                        "masters": [
                            "dm-0"
                        ],
                        "uuids": []
                    },
                    "sectors": "23064576",
                    "sectorsize": 512,
                    "size": "11.00 GB",
                    "start": "4198400",
                    "uuid": null
                }
            },
            "removable": "0",
            "rotational": "0",
            "sas_address": null,
            "sas_device_handle": null,
            "scheduler_mode": "mq-deadline",
            "sectors": "41943040",
            "sectorsize": "512",
            "size": "20.00 GB",
            "support_discard": "0",
            "vendor": "VMware",
            "virtual": 1
        },
        "sdb": {
            "holders": [],
            "host": "Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)",
            "links": {
                "ids": [],
                "labels": [],
                "masters": [],
                "uuids": []
            },
            "model": "Virtual disk",
            "partitions": {},
            "removable": "0",
            "rotational": "0",
            "sas_address": null,
            "sas_device_handle": null,
            "scheduler_mode": "mq-deadline",
            "sectors": "20971520",
            "sectorsize": "512",
            "size": "10.00 GB",
            "support_discard": "0",
            "vendor": "VMware",
            "virtual": 1
        },
        "sr0": {
            "holders": [],
            "host": "SATA controller: VMware SATA AHCI controller",
            "links": {
                "ids": [
                    "ata-VMware_Virtual_SATA_CDRW_Drive_00000000000000000001"
                ],
                "labels": [],
                "masters": [],
                "uuids": []
            },
            "model": "VMware SATA CD00",
            "partitions": {},
            "removable": "1",
            "rotational": "1",
            "sas_address": null,
            "sas_device_handle": null,
            "scheduler_mode": "mq-deadline",
            "sectors": "2097151",
            "sectorsize": "512",
            "size": "1024.00 MB",
            "support_discard": "0",
            "vendor": "NECVMWar",
            "virtual": 1
        }
}
"""

class Test_PartitioningData(unittest.TestCase):
    def setUp(self):
        self.f = FilterModule()
        self.desired = json.loads(current_state)
        self.current = yaml.load(desired_state, Loader=yaml.FullLoader)
        self.pv_present = json.loads(ansible_devices)
        self.mounts = json.loads(ansible_mounts)

    def test_vg_root(self):
        """
        Unchanged items must remain unchanged
        """
        diff = self.f.lvmdiff(self.desired, self.current, self.pv_present, self.mounts)
        vgroot = diff.find(VolumeGroup("VG_root"))
        assert len(vgroot.lvs) == 1
        assert len(vgroot.pvs) == 1
        assert vgroot.size == 11.00
        assert vgroot.free == 0
        assert vgroot.state == "present"
        assert vgroot.result == "0"
        pvs_checked = 0
        for pv in vgroot.pvs.values():
            assert pv.state == "present"
            if pv.name == "/dev/sda3":
                assert pv.size == 11.00
                assert pv.free == 0
                pvs_checked += 1
        assert pvs_checked == 1

        lvs_checked = 0
        for lv in vgroot.lvs.values():
            assert lv.state == "present"
            if lv.name == "lv_root":
                assert lv.size == 11.00
                assert lv.fs == "xfs"
                assert lv.mount == "/"
                lvs_checked += 1
        assert lvs_checked == 1        

    def test_vg_data(self):
        """
        PV is missing, space exceeded
        """
        diff = self.f.lvmdiff(self.desired, self.current, self.pv_present, self.mounts)
        vgdata = diff.find(VolumeGroup("VG_data"))
        assert vgdata.result == "1"
        

if "__main__" == __name__:
    unittest.main()
