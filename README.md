[![pipeline status](https://gitlab.ethz.ch/ansible-community/collections/system_configuration/badges/master/pipeline.svg)](https://gitlab.ethz.ch/ansible-community/collections/system_configuration)

# Ansible Collection - ethz.system_configuration

Collection of system configuration roles specific for ETH Zurich:

 - `private_subnet_client`: set HTTP proxy for OS and Docker service
 - `console_configuration`: configure system console for Swiss keyboards (and others)
 - `timeservice`: timezone, `chrony` and `ntp` settings for Zurich
 - `rsyslog_forwarder`: Configure one or more rsyslog target hosts
 - `docker_installation`: currently for RHEL7 only
 - `mail_relay`: configure standard ETHZ mail relay server for forwading local mails

# Installation
## Ansible 2.10 and later

Create a `requirements.yml' and add this git repository:

```
---
roles:
  - src: marcelnijenhof.firewalld

collections:
  - name: git+https://gitlab.ethz.ch/ansible-community/collections/system_configuration,1.0.0
```

Install with
```
ansible-galaxy collection install -r requirements.yml
```

For the time being, install `pip netaddr==0.10.0` on the Ansible controller ([see issue here](https://stackoverflow.com/questions/78180645/ansible-ansible-utils-ipaddrprivate-fails-with-attributeerror-ipnetwork)).

## Usage

Refer to the complete namespaced name, i. e.

```
- name: Set timezone and time servers
  include_role: ethz.system_configuration.timeservice
```
