Configure Timeservices
======================

Set time servers and time zone for location Zurich.

See `defaults/main.yml` for configurable parameters.

Distributions tested with Molecule:

 - CentOS 7/8
 - Debian 10 Buster
 - Ubuntu 20.04


Chrony/NTP
----------

For RHEL7, the default time service is _chrony_, and _ntp_ for RHEL6; RHEL8 support _chrony_ only. Overwrite _timeservice_selection_ to select manually.

Default time servers are defined in `defaults/main.yml`:

 - time.ethz.ch
 - time1.ethz.ch
 - time2.ethz.ch

These servers work also for private subnets.

Chrony can also use a pool like `ch.pool.ntp.org`.


Timezone
--------

Default 'Europe/Zurich' should fit most locations...
