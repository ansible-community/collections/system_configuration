http_proxy="{{ private_subnet_httpproxy }}"
https_proxy="{{ private_subnet_httpsproxy }}"
HTTP_PROXY="{{ private_subnet_httpproxy }}"
HTTPS_PROXY="{{ private_subnet_httpsproxy }}"
no_proxy="{{ private_subnet_noproxy }}"
export http_proxy https_proxy HTTP_PROXY HTTPS_PROXY no_proxy
