# Ansible Collection - ethz.private_subnet_client

Proxy Configuration for Private Subnet Clients
==============================================

Sets environment variables for HTTP and HTTPS variables, and extends the
systemd service for the Docker service.

Will skip tasks if a public address is detected.

Variables and Defaults
---------

 - `private_subnet_httpproxy`: http://proxy.ethz.ch:3128
 - `private_subnet_httpsproxy`: http://proxy.ethz.ch:3128
 - `private_subnet_noproxy`: "$no_proxy, 127.0.0.1, localhost, .ethz.ch"
 - `private_subnet_force`: Set proxy in any case