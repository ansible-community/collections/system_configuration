Basic Docker Installation
=========================

For RHEL7.

Package Source
--------------

For RHEL, you may use either the package provided by Redhat or sync the docker from docker.com to the Satellite. It will be available as <ORGANIZATION>_<PRODUCTNAME>_<REPONAME>, i.e. ID-CD_docker-ce_docker-ce-7, if you sync https://download.docker.com/linux/centos/7/x86_64/stable as repo 'docker-ce-7'.

Set *docker_rhel_package_source* to *redhat* or *upstream*. For docker-ce, provide the docker repo name as described above. For redhat, the Extras repository will be used.


Variables
---------

 - docker_rhel_package_source: defaults to *docker* for Red Hat Extras repo, *docker-ce* for upstream repo.
 
 - docker_satellite_repo: set i.e. to ID-CD_docker-ce_docker-ce-7 if using packages from docker.com via Satellite
 
 - docker_enable_proxy: add proxy support for docker service, if you are in an internal network and want to pull from outside

 - docker_cleanup_job: true. Installs a daily job to remove obsolete images and volumes.
 