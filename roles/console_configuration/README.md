Ansible Collection - console_configuration
=========

Set some basic console properties.


Role Variables
--------------

 - `consolelayout`: keyboard layout on console; default: `sg-latin1`
 - `consolefont`: console font; default: `latarcyrheb-sun16`


Installation
------------

Install the latest version:
```
ansible-galaxy install git+https://gitlab.ethz.ch/ansible-community/collections/console_configuration
```

or choose a specific version with:
```
ansible-galaxy install git+https://gitlab.ethz.ch/ansible-community/collections/console_configuration,1.0.0
```

Ansible >= 2.10 only: for use in a playbook, you may use a 
[https://galaxy.ansible.com/docs/using/installing.html](requirements file):

```
---
collections:
  - name: git+https://gitlab.ethz.ch/ansible-community/collections/console_configuration
```


License
-------

MIT
