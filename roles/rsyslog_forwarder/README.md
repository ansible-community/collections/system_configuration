rsyslog_forwarder
=========

Configure rsyslog client functionality, supports multiple log receiver targets.

Role Variables
--------------

### Global Settings

These settings may be overwritten on a per receiver base, these are the defaults:

- rsyslog_forward_protocol: tcp
- rsyslog_forward_port: 10514
- rsyslog_forward_caching: true
- rsyslog_forward_caching_todisk: false
- rsyslog_forward_caching_retrycount: -1
- rsyslog_forward_caching_queuetype: linkedList
- rsyslog_forward_caching_queuesize: 50000
- rsyslog_forward_caching_disksize: 16m

### RSyslog Server

Targetted servers are supplied as a list:

```
rsyslog_remote_targets:
  - name: example.org
    active: true
    protocol: tcp
    caching: true
    caching_todisk: false
    caching_retrycount: 100
    caching_queuetype: linkedList
    caching_disksize: 1m
```

### Local Targets

By default, no local targets are configured. You may set

```
rsyslog_local_targets_active: true
```

and supply/change the list of targets. The default targets are:

```
rsyslog_local_targets:
  - /var/log/messages: "*.info;mail.none;authpriv.none;cron.none"
  - /var/log/secure: "authpriv.*"
  - /var/log/maillog: "mail.*"
  - /var/log/cron: "cron.*"
  - ":omusrmsg:*": "*.emerg"
  - /var/log/spooler: "uucp,news.crit"
  - /var/log/boot.log: "local7.*"
```

