Disk Management Role
==============

This role will create / remove a certain logical volume (LV), volume group (VG) or physical volume (PV).
It will also create / removes the mountpoint and filesystems

Usage
-----

Used variables:

 - disk_management_lvm_configuration: used for partition based on the lvm abstraction
 - disk_management_disk_label: choose one of the following disk labels to use: aix, amiga, bsd, dvh, gpt, loop, mac, msdos, pc98, sun. Default: msdos
 - disk_management_fix_xfs_growfs: there is a bug in certain xfs_growfs package versions. Some versions needed a mount point rather than the device. To fix the issue set the variable to true, Default: false
 - disk_management_infrastructure: this variable is used to define on which infrastructure the playbook is runned. Default: vmware
 - disk_management_timeservice: this variable is used, when /var/* will be moved to another location. Choose between chronyd or ntpd. Default: chronyd

Special mounts supported for extraction:

 - /home
 - /tmp
 - /var
 - /var/lib

Otherwise the module will check if the mount directory is empty and fails if it's not. The swap is also available for extending.

Please provide the full disk and partitioning structure (examples in defaults/main.yml) since it will remove it otherwise except these mountpoints which are none removable:

 - /
 - /home
 - /tmp
 - /usr
 - /var
 - /var/log
 - /opt
 - swap

Supported opperations:
 - Adding / extending / removing volume groups with lvm
 - Adding / extending physical volumes and logical volumes

Supported filesystem types (found in https://docs.ansible.com/ansible/latest/modules/filesystem_module.html)

Supported OS: RedHat

Limitations:

 - Only one logical volume per volume group allowed

Testing
-------

Plugins are not found if they are *locally* in a collection only. They have to be installed in a directory searched by Ansible. This is a problem with automated testing, since the role uses a filter plugin from this collection.

Preinstall the collection from Git is tricky too, because different user contexts are used. Currently, the only working approach was to manually install the collection for the root user on the machine before running the tests.
