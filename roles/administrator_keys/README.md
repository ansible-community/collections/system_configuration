Role Name
=========

Deploy SSH keys, and remove obsolete keys.

Role Variables
--------------

 - `admin_keys`: list of complete keys to be added
 - `admin_obsolete_keys`: list of keys to be removed
 - `admin_user`: destination user, defaults to *root*

Example
-------

This example deploys keys for different administrator groups:

```
---
- hosts: ansible_managed
  collections:
    - ethz.system_configuration
  roles:
    - role: administrator_keys
      vars:
        admin_keys:
          - "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAA superadmin@ethz.ch"

    - role: administrator_keys
      vars:
        admin_keys:
          - "ssh-rsa AAAABBBBBBBBBBBBBBBBBBBB another_admin@ethz.ch"
```

As usual, there are different approaches. You also may use the role in a list of tasks, where you include a vars file with a set of key, include the role, include another vars file with a different set of keys, and apply the role again.
