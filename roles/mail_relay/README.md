# Mail Configuration for Server

For security reasons, mails to the local root user should be forwarded to a real address. 

This role install the _postfix_ mail transport agent, configures a mail relay server, and sets an alias for the local root user.

You **must** configure the variable `mailer_rootmail_recipient`.
