Role Name
=========

Install a list of OS packages.

Role Variables
--------------

 - `base_packages`: package names common to both RPM and DEB based systems
 - `base_deb_packages`: package names unique to DEB based systems
 - `base_rpm_packages`: package names unique to RPM based systems
 - `base_obsolete_packages`: packages to be removed

If you deploy older and newer variants of an OS like RHEL7 and RHEL8, packages may be absent or renamed in one or the other release. Use variables like this as workaround:

```
- name: My playbook
  hosts: all
  tasks:
    - name: Get list for all RHEL releases
      include_vars: packages.yml
      
    - name: Install packages
      include_role: base_packages
      
    - name: Get lists for specific RHEL releases
      include_vars: "{{ ansible_os_family | lower }}.yml"

    - name: Install packages
      include_role: base_packages
```
